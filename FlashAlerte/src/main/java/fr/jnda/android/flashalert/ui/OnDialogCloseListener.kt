package fr.jnda.android.flashalert.ui

interface OnDialogCloseListener {
    fun onDialogClose()
}